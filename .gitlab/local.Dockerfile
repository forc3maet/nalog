FROM python:3.9-bullseye

WORKDIR /app
ENV PYTHONPATH=/app:$PYTHONPATH

RUN apt-get update && \
    apt-get install -y nano bash curl

COPY reqs.txt ./

RUN pip install --upgrade pip && \
    pip install -r reqs.txt && \
    rm reqs.txt

ENTRYPOINT ["python", "/app/web.py"]
