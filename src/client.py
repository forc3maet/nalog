import os
import json
import requests
import logging
from datetime import datetime
from dotenv import load_dotenv
from flask_restful import Resource

logger = logging.getLogger("main")


class NalogRu(Resource):
    HOST = 'irkkt-mobile.nalog.ru:8888'
    DEVICE_OS = 'iOS'
    CLIENT_VERSION = '2.25.0'
    DEVICE_ID = '7C82010F-16CC-446B-8F66-FC4080C66521'
    ACCEPT = '*/*'
    USER_AGENT = 'Proverka ceka/2.25.0 (iPhone; iOS 15.6.1; Scale/3.00)'
    ACCEPT_LANGUAGE = 'ru-RU;q=1, en-US;q=0.9'
    SESSION_ID = None
    PROXIES = {}
    HEADERS = {}
    RESPONSE = {"status": 204, "message": "Nalog.Ru not available"}

    def __init__(self):
        load_dotenv()
        self.set_session_id()

    def __name__(selfs):
        time = datetime.now()
        return f'{time}'

    def set_session_id(self) -> None:
        """
        Authorization using INN and password of user lk nalog.ru
        """
        if os.getenv('CLIENT_SECRET') is None:
            raise ValueError('OS environments not content "CLIENT_SECRET"')
        if os.getenv('INN') is None:
            raise ValueError('OS environments not content "INN"')
        if os.getenv('PASSWORD') is None:
            raise ValueError('OS environments not content "PASSWORD"')
        if os.getenv('PROXY') is not None:
            self.PROXIES = {'http': os.getenv('PROXY'), 'https': os.getenv('PROXY')}

        url = f'https://{self.HOST}/v2/mobile/users/lkfl/auth'
        payload = {
            'inn': os.getenv('INN'),
            'client_secret': os.getenv('CLIENT_SECRET'),
            'password': os.getenv('PASSWORD')
        }
        self.HEADERS = {
            'Host': self.HOST,
            'Accept': self.ACCEPT,
            'Device-OS': self.DEVICE_OS,
            'Device-Id': self.DEVICE_ID,
            'clientVersion': self.CLIENT_VERSION,
            'Accept-Language': self.ACCEPT_LANGUAGE,
            'User-Agent': self.USER_AGENT,
        }

        response = requests.post(url, json=payload, headers=self.HEADERS, proxies=self.PROXIES)

        if not response.ok:
            logger.exception(f"Host doesn't respond.\n P{response.content}")
            return None

        self.SESSION_ID = response.json()['sessionId']
        self.HEADERS['sessionId'] = self.SESSION_ID

    def _get_ticket_id(self, qr: str) -> str:
        """
        Get ticker id by info from qr code

        :param qr: text from qr code. Example "t=20200727T174700&s=746.00&fn=9285000100206366&i=34929&fp=3951774668&n=1"
        :return: Ticket id. Example "5f3bc6b953d5cb4f4e43a06c"
        """
        url = f'https://{self.HOST}/v2/ticket'
        payload = {'qr': qr}

        if self.SESSION_ID is None:
            return None

        response = requests.post(url, json=payload, headers=self.HEADERS, proxies=self.PROXIES)

        if response.ok:
            return response.json()["id"]
        else:
            logger.error('{"response":' + response.content + '}')

    def get(self, qr: str) -> dict:
        """
        Get JSON ticket

        :param qr: text from qr code. Example "t=20201120T2104&s=539.94&fn=9280440300543578&i=61061&fp=939141379&n=1"
        :return: JSON ticket
        """

        ticket_id = self._get_ticket_id(qr)
        url = f'https://{self.HOST}/v2/tickets/{ticket_id}'

        if ticket_id is None:
            return self.RESPONSE

        response = requests.get(url, headers=self.HEADERS, proxies=self.PROXIES)

        if response.ok:
            logger.info('{"response":' + str(response.json()) + '}')
            return response.json()
        else:
            logger.error('{"response":' + response.content + '}')
            return self.RESPONSE

if __name__ == '__main__':
    client = NalogRu()
    qr_code = "t=20230626T2052&s=530.41&fn=9960440503166010&i=46419&fp=801206708&n=1"
    ticket = client.get(qr_code)
    print(json.dumps(ticket, indent=4, ensure_ascii=False))
