import os, sys
import json_logging, logging
from flask import Flask, redirect
from flask_restful import Api
from client import NalogRu
from dotenv import load_dotenv


app = Flask(__name__)

json_logging.init_flask(enable_json=True)
json_logging.init_request_instrument(app)

# init the logger as usual
logger = logging.getLogger("main")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

load_dotenv()
app.secret_key = os.getenv('SECRET_KEY')
DEBUG = os.getenv('DEBUG', "True") == "True"
api = Api(app)

api.add_resource(NalogRu, '/qr/<string:qr>')

@app.route('/')
def redirect_to_readme():
    return redirect("https://gitlab.com/forc3maet/nalog/-/blob/master/README.MD", code=302)


if __name__ == "__main__":
    app.run(debug=DEBUG, host='0.0.0.0', port=os.getenv('PORT', 5000))
