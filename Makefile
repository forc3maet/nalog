# See:
# http://www.gnu.org/software/make/manual/make.html
# http://linuxlib.ru/prog/make_379_manual.html

# Set shell interpreter
SHELL := /bin/bash

local:
	docker compose down && \
	docker compose build && \
	docker compose up -d

config:
	envsubst < .env.sample > .env

test: config
	docker compose run --rm --entrypoint=python3 web /app/client.py

production: config
	docker compose -f docker-compose.prod.yaml down && \
	docker compose -f docker-compose.prod.yaml build && \
	docker compose -f docker-compose.prod.yaml up -d